library blur_view;

import 'BlurView.dart';
import 'package:flutter/material.dart';
export 'BlurView.dart';

Future navigateTo(BuildContext c, Widget w, {bool opaque: false, bool oldMethod: false}) async{
  if(oldMethod){
    await Navigator.of(c).push(PageRouteBuilder(
        transitionDuration: Duration(seconds: 0),
        opaque: opaque,
        pageBuilder: (BuildContext context, _, __) {
          return w;
        }));
  }else{
    await showDialog<void>(context: c, builder: (c) => w);
  }

}