import 'dart:ui';

import 'package:flutter/material.dart';

class BlurContainer extends StatefulWidget {
  Widget? child;

  BlurContainer({this.child});

  @override
  _BlurContainerState createState() => _BlurContainerState();
}

class _BlurContainerState extends State<BlurContainer> {
  @override
  Widget build(BuildContext context) {
    return new BackdropFilter(
        filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
        child: new Container(
          decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
          child: widget.child,
        ));
  }
}

class BlurAnimationContainer extends StatefulWidget {

  Widget? child;
  Animation<double>? animation;


  BlurAnimationContainer({this.child, this.animation});

  @override
  _BlurAnimationContainerState createState() => _BlurAnimationContainerState();
}

class _BlurAnimationContainerState extends State<BlurAnimationContainer> {
  @override
  Widget build(BuildContext context) {
    return new BackdropFilter(
        filter: new ImageFilter.blur(sigmaX: widget.animation!.value, sigmaY: widget.animation!.value,),
        child: new Container(
          decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
          child: widget.child,
        ));
  }
}
