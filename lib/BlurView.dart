import 'package:flutter/material.dart';
import 'BlurContainer.dart';

class BlurView extends StatefulWidget {
  Widget? child;
  Key? key;
  Duration duration;
  double opacity_end;
  BlurView(
      {this.key, this.child, this.duration: const Duration(milliseconds: 200), this.opacity_end: 160.0});

  @override
  BlurViewState createState() => BlurViewState();
}

class BlurViewState extends State<BlurView>
    with SingleTickerProviderStateMixin {
  Animation<double>? fadeAnimation;
  Animation<double>? opacityAnimation;
  Animation<double>? blurAnimation;
  AnimationController? controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: widget.duration, vsync: this);
    fadeAnimation = Tween<double>(begin: 0, end: 1.0).animate(controller!)
      ..addListener(() {
        setState(() {});
      });
    opacityAnimation = Tween<double>(begin: 30, end: widget.opacity_end).animate(controller!)
      ..addListener(() {
        setState(() {});
      });
    blurAnimation = Tween<double>(begin: 0.0, end: 10.0).animate(controller!)
      ..addListener(() {
        setState(() {});
      });
    controller!.forward();
  }

  @override
  Widget build(BuildContext context) {
    var cont =
        BlurAnimationContainer(child: widget.child, animation: blurAnimation);
    return Material(
      color: Colors.black.withAlpha((opacityAnimation!.value).toInt()),
      child: FadeTransition(opacity: fadeAnimation!, child: cont),
    );
  }

  void close() async {
    await controller!.reverse();
    Navigator.pop(context);
  }

  void reverse() async {
    await controller!.reverse();
  }

  @override
  void dispose() {
    controller!.dispose();
    super.dispose();
  }
}
